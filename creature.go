package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
)

// Creatures is a collection of Creature
type Creatures []Creature

// Creature is what we're tracking feedings for
type Creature struct {
	gorm.Model
	Name      string `json:"name" gorm:"unique"` // Creature name
	Food      string `json:"food"`               // Creature consumes this
	LastFed   string `json:"last_fed"`           // Last time creature was fed
	Frequency int    `json:"frequency"`          // How often creature needs feeding (days)
	Image     string `json:"image"`              // FileName for creature image
	Due       bool   `json:"due"`                // Is the creature due to be fed
}

func (s *server) create(w http.ResponseWriter, r *http.Request) {
	var creature Creature
	err := json.NewDecoder(r.Body).Decode(&creature)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf(`{ "message": "Error Unmarshaling JSON" }`)))
		return
	}

	if result := s.db.Create(&creature); result.Error != nil {
		respJSON := []byte(fmt.Sprintf(`{ "message": "Insert Error: %v" }`, result.Error))
		w.WriteHeader(http.StatusConflict)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusCreated)
	json.NewEncoder(w).Encode(creature)
}

func (s *server) read(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var creature Creature
	s.db.Where("ID = ?", id).First(&creature)

	if creature.Name == "" {
		respJSON := []byte(fmt.Sprintf(`{ "message": "Creature not found" }`))
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(creature)

}

func (s *server) update(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var creatureUpdate Creature
	err := json.NewDecoder(r.Body).Decode(&creatureUpdate)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(fmt.Sprintf(`{ "message": "Error Unmarshaling JSON" }`)))
		return
	}

	var creature Creature
	affected := s.db.Where("ID = ?", id).First(&creature).Update(&creatureUpdate).RowsAffected

	if affected < 1 {
		respJSON := []byte(fmt.Sprintf(`{ "message": "Creature not found" }`))
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(creature)
}

func (s *server) delete(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var creature Creature
	s.db.Where("ID = ?", id).First(&creature)

	if creature.Name == "" {
		respJSON := []byte(fmt.Sprintf(`{ "message": "Creature not found" }`))
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	s.db.Delete(&creature)

	w.WriteHeader(http.StatusOK)
}

func (s *server) readAll(w http.ResponseWriter, r *http.Request) {
	var creatures []Creature

	s.db.Find(&creatures)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(creatures)
}

func (s *server) feedCreature(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	var creature Creature
	s.db.Where("ID = ?", id).First(&creature)

	if creature.Name == "" {
		respJSON := []byte(fmt.Sprintf(`{ "message": "Creature not found" }`))
		w.WriteHeader(http.StatusNotFound)
		w.Write(respJSON)
		return
	}

	now := time.Now().Format("Mon, Jan_2 3:04pm")
	creature.LastFed = now

	s.db.Save(&creature)

	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(creature)
}

module gitlab.com/t1960ct/feedings

go 1.13

require (
	github.com/alexbyk/panicif v1.0.2
	github.com/gorilla/mux v1.7.3
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jinzhu/gorm v1.9.11
	github.com/konsorten/go-windows-terminal-sequences v1.0.2 // indirect
	github.com/mattn/goveralls v0.0.4 // indirect
	github.com/pkg/errors v0.8.0
	github.com/rs/cors v1.7.0
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/objx v0.2.0 // indirect
	github.com/stretchr/testify v1.4.0
	golang.org/x/sys v0.0.0-20191120155948-bd437916bb0e // indirect
	golang.org/x/tools v0.0.0-20191206204035-259af5ff87bd // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
)

# Feed Tracker

[![Coverage Status](https://coveralls.io/repos/gitlab/T1960CT/feedtracker/badge.svg?branch=%28HEAD+detached+at+e3c2c51%29)](https://coveralls.io/gitlab/T1960CT/feedtracker?branch=%28HEAD+detached+at+e3c2c51%29)

### Description
This program aims to help keep track of creatures that need regular feeding.

My own use case sees this used for animals (reptiles, birds, fish, arachnides, insects), 
as well as some plants, and bacterial cultures like Yeast starters (for sourdough bread),
Kombucha SCOBYs (fermented tea culture) and cider vinegars. 

This is why I refer to the object in the server as "creature" instead of "animal," 
and also because others' uses may differ from animal specific scheduling.
 
### Sample Data
POST to /creature/create
```json
{
	"name": "Rosie",
	"food": "Rats",
	"frequency":9,
	"image":"rosie.jpg"
}
```

### Getting started (WIP)
Images are located in the `/images` directory, and are placed there manually (for now). 
When creating a creature, the filename will be entered in the "Image" field

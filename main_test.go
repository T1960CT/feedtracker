package main

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCreatesServer(t *testing.T) {
	assert := assert.New(t)
	srv := createServer("./test.db")
	assert.NotNil(srv.db)
	assert.NotNil(srv.router)

	err := DeleteTestDB()
	assert.NoError(err)
}

func DeleteTestDB() error {
	if err := os.Remove("./test.db"); err != nil {
		return err
	}
	return nil
}

package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/stretchr/testify/assert"
)

type TestCreature struct {
	gorm.Model
	Name      string    `json:"name" gorm:"unique"` // Creature name
	Food      string    `json:"food"`               // Creature consumes this
	LastFed   time.Time // Last time creature was fed
	Frequency int       `json:"frequency"` // How often creature needs feeding (days)
	Image     string    `json:"image"`     // FileName for creature image
}

// CREATE

func TestCreatureCreateNoJSON(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	jsonCreature := []byte(``)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, req.StatusCode)

	err = DeleteTestDB()
	assert.NoError(err)
}

func verifyResponseData(t *testing.T, r *http.Response) {
	assert := assert.New(t)

	var responseCreature Creature
	err := json.NewDecoder(r.Body).Decode(&responseCreature)
	assert.NoError(err)
	assert.Equal(1, int(responseCreature.ID))
	assert.Equal("test", responseCreature.Name)
	assert.Equal("test", responseCreature.Food)
	assert.Equal(1, responseCreature.Frequency)
	assert.Equal("image.png", responseCreature.Image)
}

func TestCreatureCreate(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	creature := Creature{
		Name:      "test",
		Food:      "test",
		Frequency: 1,
		Image:     "image.png",
	}

	jsonCreature, err := json.Marshal(creature)
	assert.NoError(err)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	verifyResponseData(t, req)

	req, err = http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusConflict, req.StatusCode)
	err = DeleteTestDB()
	assert.NoError(err)
}

// READ

func TestCreatureReadNotExist(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	req, err := http.Get(httpServer.URL + "/creature/find/999")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	err = DeleteTestDB()
	assert.NoError(err)
}
func TestCreatureRead(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	creature := Creature{
		Name:      "test",
		Food:      "test",
		Frequency: 1,
		Image:     "image.png",
	}

	jsonCreature, err := json.Marshal(creature)
	assert.NoError(err)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/creature/find/1")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	verifyResponseData(t, req)

	err = DeleteTestDB()
	assert.NoError(err)
}

// ALL

func TestCreatureReadAll(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	creature := Creature{
		Name:      "test",
		Food:      "test",
		Frequency: 1,
		Image:     "image.png",
	}

	jsonCreature, err := json.Marshal(creature)
	assert.NoError(err)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/creatures")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	var creatures []Creature
	err = json.NewDecoder(req.Body).Decode(&creatures)
	assert.NoError(err)
	assert.Len(creatures, 1)

	err = DeleteTestDB()
	assert.NoError(err)
}

// DELETE

func TestCreatureDelete(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	creature := Creature{
		Name:      "test",
		Food:      "test",
		Frequency: 1,
		Image:     "image.png",
	}

	jsonCreature, err := json.Marshal(creature)
	assert.NoError(err)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	client := &http.Client{}

	req2, err := http.NewRequest("DELETE", httpServer.URL+"/creature/1", nil)
	assert.NoError(err)
	resp, err := client.Do(req2)
	assert.NoError(err)
	defer resp.Body.Close()
	assert.Equal(http.StatusOK, resp.StatusCode)

	req3, err := http.NewRequest("DELETE", httpServer.URL+"/creature/9999", nil)
	assert.NoError(err)
	resp, err = client.Do(req3)
	assert.NoError(err)
	defer resp.Body.Close()
	assert.Equal(http.StatusNotFound, resp.StatusCode)

	req, err = http.Get(httpServer.URL + "/creature/find/1")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	err = DeleteTestDB()
	assert.NoError(err)
}

// UPDATE

func TestCreatureUpdate(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	creature := Creature{
		Name:      "test",
		Food:      "test",
		Frequency: 1,
		Image:     "image.png",
	}

	jsonCreature, err := json.Marshal(creature)
	assert.NoError(err)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	creature2 := Creature{
		Name:      "new",
		Food:      "new",
		Frequency: 9,
		Image:     "new.png",
	}

	jsonCreature, err = json.Marshal(creature2)
	assert.NoError(err)

	req, err = http.Post(httpServer.URL+"/creature/update/1", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	//assert.Equal(http.StatusOK, req.StatusCode)

	var responseCreature Creature
	err = json.NewDecoder(req.Body).Decode(&responseCreature)
	assert.NoError(err)
	assert.Equal("new", responseCreature.Name)

	req, err = http.Post(httpServer.URL+"/creature/update/9999", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	err = DeleteTestDB()
	assert.NoError(err)
}

func TestCreatureUpdateBadJSON(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	jsonCreature := []byte(``)

	req, err := http.Post(httpServer.URL+"/creature/update/1", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusBadRequest, req.StatusCode)

	err = DeleteTestDB()
	assert.NoError(err)
}

func TestCreatureFeed(t *testing.T) {
	assert := assert.New(t)

	srv := createServer("./test.db")
	defer srv.db.Close()

	httpServer := httptest.NewServer(srv.router)

	creature := Creature{
		Name:      "test",
		Food:      "test",
		Frequency: 1,
		Image:     "image.png",
	}

	jsonCreature, err := json.Marshal(creature)
	assert.NoError(err)

	req, err := http.Post(httpServer.URL+"/creature/create", "application/json", bytes.NewBuffer(jsonCreature))
	assert.NoError(err)
	assert.Equal(http.StatusCreated, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/creature/feed/1")
	assert.NoError(err)
	assert.Equal(http.StatusOK, req.StatusCode)

	req, err = http.Get(httpServer.URL + "/creature/feed/9999")
	assert.NoError(err)
	assert.Equal(http.StatusNotFound, req.StatusCode)

	err = DeleteTestDB()
	assert.NoError(err)
}

package main

import (
	"net/http"

	"github.com/alexbyk/panicif"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
)

type server struct {
	//router *mux.Router
	router http.Handler
	db     *gorm.DB
}

func main() {
	srv := createServer("./creatures.db")
	defer srv.db.Close()
	http.ListenAndServe(":8081", srv.router)
}

func createServer(dbName string) *server {
	srv := &server{}
	srv.createDatabase(dbName)
	srv.createRouting()
	logrus.Info("Server created!")
	return srv
}

func (s *server) createDatabase(dbName string) {
	db, err := gorm.Open("sqlite3", dbName)
	db.AutoMigrate(&Creature{})
	panicif.Err(err)
	s.db = db
}

func (s *server) createRouting() {
	router := mux.NewRouter()
	// CRUD for creatures
	router.HandleFunc("/creature/create", s.create).Methods("POST")
	router.HandleFunc("/creature/update/{id}", s.update).Methods("POST")
	router.HandleFunc("/creature/{id}", s.delete).Methods("DELETE")
	router.HandleFunc("/creature/find/{id}", s.read).Methods("GET")
	router.HandleFunc("/creatures", s.readAll).Methods("GET")
	router.HandleFunc("/creature/feed/{id}", s.feedCreature).Methods("GET")

	handler := cors.Default().Handler(router)

	s.router = handler
}
